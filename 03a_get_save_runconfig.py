#!/home/cisco/GIT/venv-vz/bin/python
#
# Get Run Config from CSR Router via Netconf and Save it to a file on localserver
# will create running.xml file

from ncclient import manager
import xml.dom.minidom

# use the IP address or hostname of your IOS-XE device
HOST = '172.20.1.50'
# use the NETCONF port for your IOS-XE device
PORT = 830
# use the user credentials for your IOS-XE device
USER = 'cisco'
PASS = 'cisco'

# Filtering for specific config
intfilter = """
<filter xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
<native 
    xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native">
    <interface>
        <GigabitEthernet>
        </GigabitEthernet>
    </interface>
</native>
</filter>"""

rtrmgr = manager.connect(host=HOST, port=PORT, username=USER, password=PASS,
                         hostkey_verify=False, device_params={'name': 'csr'},
                         allow_agent=False, look_for_keys=False)

# Get full config
runcfg = rtrmgr.get_config('running')

# Get filtered config
# runcfg = rtrmgr.get_config('running', filter=intfilter)

# print(xml.dom.minidom.parseString(runcfg.xml).toprettyxml(indent = " ")) 

SAVE = open('R1_run_config.xml', 'w')
SAVE.write(str(xml.dom.minidom.parseString(runcfg.xml).toprettyxml(indent = " ")))
SAVE.close

rtrmgr.close_session()

