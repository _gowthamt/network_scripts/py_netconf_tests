#!/home/cisco/GIT/venv-vz/bin/python
#
# Save Running config to start up config on CSR Router via Netconf
#

from ncclient import manager, xml_
import xmltodict
import xml.dom.minidom

# use the IP address or hostname of your IOS-XE device
HOST = '172.20.1.51'
# use the NETCONF port for your IOS-XE device
PORT = 830
# use the user credentials for your IOS-XE device
USER = 'cisco'
PASS = 'cisco'

# Save run config to start config on cisco csr routers
SAVE = """
<cisco-ia:save-config xmlns:cisco-ia="http://cisco.com/yang/cisco-ia"/>
"""

rtrmgr = manager.connect(host=HOST, port=PORT, username=USER, password=PASS,
                         hostkey_verify=False, device_params={'name': 'csr'},
                         allow_agent=False, look_for_keys=False)

netconf_reply = rtrmgr.dispatch(xml_.to_ele(SAVE))

print(xml.dom.minidom.parseString(netconf_reply.xml).toprettyxml())
print("")

rtrmgr.close_session()

