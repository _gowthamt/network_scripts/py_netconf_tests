#!/home/cisco/GIT/venv-vz/bin/python
#
# Get Netconf Schema for specific features from CSR Router
#

from ncclient import manager

# use the IP address or hostname of your IOS-XE device
HOST = '172.20.1.50'
# use the NETCONF port for your IOS-XE device
PORT = 830
# use the user credentials for your IOS-XE device
USER = 'cisco'
PASS = 'cisco'


rtrmgr = manager.connect(host=HOST, port=PORT, username=USER, password=PASS,
                         hostkey_verify=False, device_params={'name': 'csr'},
                         allow_agent=False, look_for_keys=False)

#Example Find Schem from capabilities list
#
# http://cisco.com/ns/yang/Cisco-IOS-XE-interface-common?module=Cisco-IOS-XE-interface-common&revision=2020-07-01
#
# get_schema('Cisco-IOS-XE-interface-common')

#schema = rtrmgr.get_schema('Cisco-IOS-XE-interface-common')
schema = rtrmgr.get_schema('ietf-interfaces')
print (schema)

rtrmgr.close_session()

