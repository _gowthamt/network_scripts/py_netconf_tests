#!/home/cisco/GIT/venv-vz/bin/python
#
# Parse xml file - R1_int_config.xml
# Understanding how to index xml data structure

import xml.etree.ElementTree as ET

# Open exsiting XML File and Parse the output

tree = ET.parse('R1_int_config.xml')
root = tree.getroot()

# Print root element
print(root)

# Print tag of element
print(root.tag)

# Print native of xml elment
print(root[0][0].tag)

# Print interface name of xml input
# [data][native][interface][Gigabitethernet][name]
print(root[0][0][0][0][0].text)
# [data][native][interface][Gigabitethernet][ip][address]
print(root[0][0][0][0][1].tag)

# Print interface name
int_number = list(root)[0][0][0][0][0].text
int_ip =    list(root)[0][0][0][0][2][0][0][0].text
print(int_number)
print(int_ip)

print("GigabitEthernet " + int_number + " - IP Address: " + int_ip)



# Get USER INPUT

n = int(input("Enter Interface Number: "))
n = n - 1

usrint_number = list(root)[0][0][0][n][0].text
usrint_ip = list(root)[0][0][0][n][2][0][0][0].text

print("GigabitEthernet " + usrint_number + " - IP Address: " + usrint_ip)


