#!/home/cisco/GIT/venv-vz/bin/python
#
# Simple configuration of XE (17.3.1) Router
#

from ncclient import manager
import sys
import xml.dom.minidom

# use the IP address or hostname of your IOS-XE device
HOST = '172.20.1.50'
# use the NETCONF port for your IOS-XE device
PORT = 830
# use the user credentials for your IOS-XE device
USER = 'cisco'
PASS = 'cisco'

# Configuring XE Devices
sethostname = """
<config xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
    <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native">
        <hostname>R1</hostname>
    </native>
</config>"""


# create a main() method
def set_config():
    """Main method that retrieves the interfaces from config via NETCONF."""
    with manager.connect(host=HOST, port=PORT, username=USER, password=PASS,
                         hostkey_verify=False, device_params={'name': 'default'},
                         allow_agent=False, look_for_keys=False) as m:

        return(m.edit_config(sethostname, target='running'))


def main():
    """Simple main method calling our function."""
    config = set_config()
    print(config.xml) 


if __name__ == '__main__':
    sys.exit(main())

