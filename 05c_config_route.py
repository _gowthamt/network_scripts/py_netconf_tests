#!/home/cisco/GIT/venv-vz/bin/python
#
# Get Netconf Capabilities from CSR Router
#

from ncclient import manager
import sys
import xml.dom.minidom

# use the IP address or hostname of your IOS-XE device
HOST = '172.20.1.51'
# use the NETCONF port for your IOS-XE device
PORT = 830
# use the user credentials for your IOS-XE device
USER = 'cisco'
PASS = 'cisco'

# Simpler XPATH Filtering 
#print (m.get_config('running', filter=('xpath', '/native/router/bgp')))
#print (m.get_config('running', filter=('xpath', '/native/interface')))
xpathfiler = '/native/router/bgp'

# Filtering for specific config
routefilter = """
<filter xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
<native 
    xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native">
    <ip>
        <route>
            <ip-route-interface-forwarding-list></ip-route-interface-forwarding-list>
        </route>
    </ip>
</native>
</filter>"""

# Configuring XE Devices
setroute = """
<config xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
    <native 
        xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native">
        <ip>
            <route>
                <ip-route-interface-forwarding-list>
                <prefix>10.5.113.0</prefix>
                <mask>255.255.255.0</mask>
                <fwd-list>
                    <fwd>172.20.1.1</fwd>
                </fwd-list>
                </ip-route-interface-forwarding-list>
            </route>
        </ip>
    </native>
</config>"""


# create a main() method
def get_config():
    """Main method that retrieves the interfaces from config via NETCONF."""
    with manager.connect(host=HOST, port=PORT, username=USER, password=PASS,
                         hostkey_verify=False, device_params={'name': 'default'},
                         allow_agent=False, look_for_keys=False) as m:
        
        print("\n Get Netconf Config \n")

        # Option 1: filter using xpath
        # return(xml.dom.minidom.parseString(str(m.get_config(source='running', filter=('xpath', xpathfiler)))))
        
        # Option 2: Filter using xml definition
        return(xml.dom.minidom.parseString(str(m.get_config(source='running', filter=routefilter))))

# create a main() method
def set_config():
    """Main method that retrieves the interfaces from config via NETCONF."""
    with manager.connect(host=HOST, port=PORT, username=USER, password=PASS,
                         hostkey_verify=False, device_params={'name': 'default'},
                         allow_agent=False, look_for_keys=False) as m:
        print("\n Set Netconf Config \n")
        return(m.edit_config(setroute, target='running'))


def main():
    """Simple main method calling our function."""
    interfaces = get_config()
    print(interfaces.toprettyxml(indent = " ")) 

    config = set_config()
    print(config.xml) 

    interfaces = get_config()
    print(interfaces.toprettyxml(indent = " ")) 

if __name__ == '__main__':
    sys.exit(main())

# rtrmgr.close_session()

