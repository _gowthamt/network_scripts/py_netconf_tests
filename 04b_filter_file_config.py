#!/home/cisco/GIT/venv-vz/bin/python
#
# Get Netconf Capabilities from CSR Router
#

from ncclient import manager
import sys
import xml.dom.minidom

# use the IP address or hostname of your IOS-XE device
HOST = '172.20.1.50'
# use the NETCONF port for your IOS-XE device
PORT = 830
# use the user credentials for your IOS-XE device
USER = 'cisco'
PASS = 'cisco'
# XML file to open
FILE = 'get_interfaces.xml'
# FILE = 'get_bgp_nei.xml'


# create a main() method
def get_configured_interfaces():
    """Main method that retrieves the interfaces from config via NETCONF."""
    with manager.connect(host=HOST, port=PORT, username=USER, password=PASS,
                         hostkey_verify=False, device_params={'name': 'default'},
                         allow_agent=False, look_for_keys=False) as m:

        with open(FILE) as f:
            return(m.get_config('running', f.read()))

def main():
    """Simple main method calling our function."""
    interfaces = get_configured_interfaces()
    print(xml.dom.minidom.parseString(interfaces.xml).toprettyxml(indent = " ")) 


if __name__ == '__main__':
    sys.exit(main())
