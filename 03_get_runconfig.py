#!/home/cisco/GIT/venv-vz/bin/python
#
# Get Netconf Capabilities from CSR Router
#

from ncclient import manager
import xml.dom.minidom

# use the IP address or hostname of your IOS-XE device
HOST = '172.20.1.50'
# use the NETCONF port for your IOS-XE device
PORT = 830
# use the user credentials for your IOS-XE device
USER = 'cisco'
PASS = 'cisco'


rtrmgr = manager.connect(host=HOST, port=PORT, username=USER, password=PASS,
                         hostkey_verify=False, device_params={'name': 'csr'},
                         allow_agent=False, look_for_keys=False)

runcfg = rtrmgr.get_config('running')
print(xml.dom.minidom.parseString(runcfg.xml).toprettyxml(indent = " ")) 

rtrmgr.close_session()

