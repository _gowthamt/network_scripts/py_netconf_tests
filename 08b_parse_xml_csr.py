#!/home/cisco/GIT/venv-vz/bin/python
#
# Parse xml file - R1_int_config.xml
# Understanding how to index xml data structure

import xml.etree.ElementTree as ET
from ncclient import manager
import xml.dom.minidom


# use the IP address or hostname of your IOS-XE device
HOST = '172.20.1.50'
# use the NETCONF port for your IOS-XE device
PORT = 830
# use the user credentials for your IOS-XE device
USER = 'cisco'
PASS = 'cisco'

# Filtering for specific config
intfilter = """
<filter xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
<native 
    xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native">
    <interface>
        <GigabitEthernet>
        </GigabitEthernet>
    </interface>
</native>
</filter>"""

rtrmgr = manager.connect(host=HOST, port=PORT, username=USER, password=PASS,
                         hostkey_verify=False, device_params={'name': 'csr'},
                         allow_agent=False, look_for_keys=False)

# Get full config
# runcfg = rtrmgr.get_config('running')

# Get filtered config
output = rtrmgr.get_config('running', filter=intfilter)

# print(xml.dom.minidom.parseString(runcfg.xml).toprettyxml(indent = " ")) 


# Open exsiting XML File and Parse the output

root = ET.fromstring(str(output))

# Get USER INPUT

n = int(input("Enter Interface Number: "))
n = n - 1

usrint_number = list(root)[0][0][0][n][0].text
usrint_ip = list(root)[0][0][0][n][2][0][0][0].text

print("GigabitEthernet " + usrint_number + " - IP Address: " + usrint_ip)




rtrmgr.close_session()

