#!/home/cisco/GIT/venv-vz/bin/python
#
# Get Netconf Configs from CSR Router and Set Configuration on CSR Router
#
# Python Pkgs Required - netaddr, jinja2, ncclient
# Tested on Python 3.9.9
#
# Minimum Requirement to Run - python lab_config_int_bgp.py -H 172.20.1.50  -t lab_config.j2 -i R1_config_vars.yml
# Optional - python lab_config_int_bgp.py -u cisco -p cisco -H 172.20.1.50 -P 830 -t lab_config.j2 -i R1_config_vars.yml

import argparse
import netaddr
import os.path
import re
import sys
import json
import yaml
import xml.dom.minidom
import xml.etree.ElementTree as ET
from datetime import datetime
from ncclient import manager
from jinja2 import Environment
from jinja2 import FileSystemLoader

# Do not create __pycache__ dir
sys.dont_write_bytecode = True 

# use the IP address or hostname of your IOS-XE device
HOST = '172.20.1.50'
# use the NETCONF port for your IOS-XE device
PORT = 830
# use the user credentials for your IOS-XE device
USER = 'cisco'
PASS = 'cisco'

# PARSERARGS - Jinja2 Template 
jtemplate = 'lab_config.j2'

# PARSERARGS - Router Template Variable File
rtrvars = 'R1_config_vars.yml'

# PARSERARGS - XML file to open
rtrconfig = 'R1_lab_config.xml'
# rtrconfig = 'R2_lab_config.xml'


# Simple XPATH Filtering 
xpathint = '/native/interface'
xpathbgp = '/native/router/bgp'

#---- Create a method to retrieve and parse interface operational data' -----#

def get_int_config(HOST, PORT, USER, PASS, xpath):
    """Main method that retrieves the interfaces from config via NETCONF."""
    with manager.connect(host=HOST, port=PORT, username=USER, password=PASS,
                         hostkey_verify=False, device_params={'name': 'default'},
                         allow_agent=False, look_for_keys=False) as m:
        
        print("Get Netconf Interface Config \n")

        # Option 1: filter using xpath
        # return(xml.dom.minidom.parseString(str(m.get_config(source='running', filter=('xpath', xpath)))))
    
        # # Option 2: Filter using xml definition
        # return(xml.dom.minidom.parseString(str(m.get_config(source='running', filter=routefilter))))

        try:
            # issue the edit-config operation with the XML config to enable operational data
            output = m.get_config(source='running', filter=('xpath', xpath))
        except Exception as e:
            print("Encountered the following RPC error!")
            print(e)
            sys.exit()

        root = ET.fromstring(str(output))
        n = len(list(root)[0][0][0])
        # print (n)

        for i in range(n):
            usrint_name = list(root)[0][0][0][i].tag
            usrint_number = list(root)[0][0][0][i][0].text
            if 'GigabitEthernet' in usrint_name:
                try:
                    # issue the an RPC get while on interfaces-state
                    intname = "GigabitEthernet"
                    usrint_ip = list(root)[0][0][0][i][2][0][0][0].text
                except Exception as e:
                    usrint_ip = "unassigned"
            else:
                try:
                    # issue the an RPC get while on interfaces-state
                    intname = "Loopback"
                    usrint_ip = list(root)[0][0][0][i][1][0][0][0].text
                except Exception as e:
                    usrint_ip = "unassigned"
                # usrint_ip = list(root)[0][0][0][i][1][0][0][0].text
                
            print(intname + usrint_number + " : " + usrint_ip)

        return(xml.dom.minidom.parseString(str(output)))

#---- Create a method to retrieve and parse BGP operational data -----#

def get_bgp_config(HOST, PORT, USER, PASS, xpath):
    """Main method that retrieves the interfaces from config via NETCONF."""
    with manager.connect(host=HOST, port=PORT, username=USER, password=PASS,
                         hostkey_verify=False, device_params={'name': 'default'},
                         allow_agent=False, look_for_keys=False) as m:
        
        print("\n Get Netconf BGP Config \n")

        # Option 1: filter using xpath
        # return(xml.dom.minidom.parseString(str(m.get_config(source='running', filter=('xpath', xpath)))))
        
        # # Option 2: Filter using xml definition
        # return(xml.dom.minidom.parseString(str(m.get_config(source='running', filter=routefilter))))

        try:
            # issue the edit-config operation with the XML config to enable operational data
            output = m.get_config(source='running', filter=('xpath', xpath))
        except Exception as e:
            print("Encountered the following RPC error!")
            print(e)
            sys.exit()

        root = ET.fromstring(str(output))
        n = len(list(root)[0][0][0][0])

        # print(root[0][0].tag) #-> native
        # print(root[0][0][0].tag) #-> router
        # print(root[0][0][0][0].tag) #-> bgp
        # print(root[0][0][0][0][0].text) #-> bgp id
        # print(root[0][0][0][0][2].tag) #-> neighbor
        # print(root[0][0][0][0][2][0].text) #-> neighbor address
        # print(root[0][0][0][0][2][1].text) #-> neighbor remote-as

        for i in range(2,n):
            usrbgp_nei = (root[0][0][0][0][i][0].text)
            usrbgp_remoteas = list(root)[0][0][0][0][i][1].text

            print("BGP Neigbhbor " + usrbgp_nei + " remote-as " + usrbgp_remoteas)

        return(xml.dom.minidom.parseString(str(output)))

#---- Create a method to use Jinja2 config template and generate config file -----#

def create_lab_config(hostname, template, variables):
    """Function to render XML document to configure csr1kv router from Jinja2."""
    print ("\n ===== 1. Generate XML Config ===== \n")
    varsfile = os.path.join('./router_vars/', variables)
    templatefile = os.path.join('./templates/', template)

    # Verify varsfile format
    if re.search(".yml", varsfile):
        with open(varsfile, "r") as f:
            print('Loading YAML Variable File')
            data = yaml.load(f, Loader=yaml.SafeLoader)
    elif re.search(".json", varsfile):
        with open(varsfile, "r") as f:
            print('Loading JSON Variable File')
            data = json.load(f)
    else:
        sys.exit(f"Not supported file format: {varsfile}")

    # Verify templatefile format
    if re.search(".j2", templatefile):
        print('Template file check passed')
    else:
        sys.exit(f"Template file format not supported: {templatefile}")


    # Use the Jinja2 template provided to create the XML config
    env = Environment(loader=FileSystemLoader('./templates/'))
    envtemplate = env.get_template(template)
    renderedconfig = envtemplate.render(data)

    # Save the results to the XML file
    currentDT = datetime.now()
    configfile = "./router_configs/" + hostname + "_xmlconfig_" + currentDT.strftime("%Y-%m-%d-%H-%M") + ".xml"
    
    with open(configfile, "w") as f:
        f.write(renderedconfig)

    return(configfile)

#---- Create a method to set Router interface loop, gig sub-int, and bgp nei config -----#

def set_config(HOST, PORT, USER, PASS, configfile):
    """Main method that retrieves the interfaces from config via NETCONF."""
    with manager.connect(host=HOST, port=PORT, username=USER, password=PASS,
                         hostkey_verify=False, device_params={'name': 'default'},
                         allow_agent=False, look_for_keys=False) as m:
        print("\n ===== 3. Set Netconf Config ===== \n")
        with open(configfile) as f:
            try:
                # issue the edit-config operation with the XML config to enable operational data
                rpc_reply = m.edit_config(f.read(), target='running')
            except Exception as e:
                print("Encountered the following RPC error!")
                print(e)
                sys.exit()

        # verify the RPC get was successful before continuing
        if rpc_reply.ok is not True:
            print("Encountered a problem when retrieving operational state!")
            sys.exit()
        else:
            # return the RPC reply containing interface data in XML format
            return(rpc_reply)

#==================================================================================================
# -----------    MAIN: Create Netconf Config and Apply to CSR1Kv Routers --------------
#==================================================================================================

def main():
    """Simple main method calling our function."""

    # User Input to Connect to Host
    parser = argparse.ArgumentParser()
    parser.add_argument('--template', '-t', default=jtemplate, help="Jinja2 Template file name")
    # parser.add_argument('--config', '-c', default=rtrconfig, help="XML Config file name")
    parser.add_argument('--inputfile', '-i', default=rtrvars, help="Variable Input file name")
    parser.add_argument('--user', '-u', default=USER, help="user name on remote host")
    parser.add_argument('--password', '-p', default=PASS, help="password on remote host")
    parser.add_argument('--port', '-P', default=PORT, type=int, help="port on remote host")
    parser.add_argument('--host', '-H', default=HOST, help="remote host")
    args = parser.parse_args()

    # Check for valid host IP
    try:
        ip = netaddr.IPNetwork(args.host)
    except netaddr.core.AddrFormatError as e:
        parser.print_usage()
        print(e)
        sys.exit()

    if args.host == "172.20.1.50":
        hostname = "R1"
    elif args.host == "172.20.1.51":
        hostname = "R2"
    else:
        hostname = "rtr"

    # 1. Generate Config: Use Jinja2 Template to create Interface and BGP XML Config 
    rtrconfig = create_lab_config(hostname, args.template, args.inputfile)
    # exit if the file hasn't been created
    if os.path.isfile(rtrconfig) is not True:
        print("Failed to create the XML config file!")
        sys.exit()
    else:
        print("Template file created: " + rtrconfig)

    # 2. PRE-CHECK Run: Get current Interface and BGP config on Router
    print ("\n ===== 2. Pre Capture ===== \n")
    intconfig = get_int_config(args.host, args.port, args.user, args.password, xpathint)
    # print(intconfig.toprettyxml(indent = " ")) 

    bgpconfig = get_bgp_config(args.host, args.port, args.user, args.password, xpathbgp)
    # print(bgpconfig.toprettyxml(indent = " ")) 

    # 3. Configure Router: Configure new Interface and BGP Neighbor config on Router
    config = set_config(args.host, args.port, args.user, args.password, rtrconfig)
    # print(str(config))
    result = re.search("ok", str(config))

    if result:
        print("Netconf Provisioning Success \n")
        print(config.xml)
    else:
        print("Netconf Provisioning Failed \n")
        sys.exit()

        
    # 4. POST-CHECK Run: Ensure New Interface and BGP config on Router
    print ("\n ===== 4. Post Capture ===== \n")
    intconfig = get_int_config(args.host, args.port, args.user, args.password, xpathint)
    # print(intconfig.toprettyxml(indent = " ")) 

    bgpconfig = get_bgp_config(args.host, args.port, args.user, args.password, xpathbgp)
    # print(bgpconfig.toprettyxml(indent = " ")) 

if __name__ == '__main__':
    sys.exit(main())

