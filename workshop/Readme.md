# SRE DevSecOps Workshop Lab

# **<p align="center">SRE DevSecOps Workshop Lab</p>**
---
# **<p align="center">Lab Startup File</p>**

---
### **<p align="center">CX SRE Team</p>**
### **<p align="center">March 8 2022</p>**

---
# [Lab pod assignment](./pod-assignment.md)
# [Lab access](./lab-access.md)
# [Lab guide](./Network-Automation-with-Ansible.md)
# [Lecture Slides](./LTRPRG-1500-Network-Automation-with-Ansible.pdf)
# [Playbook files](./playbooks)
# [VI reference](./vi-reference.md)

---