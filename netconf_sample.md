# Netconf Guide

## Configuring Netconf on CSR1kv
Router Config

```
aaa new-model
!
!
aaa authorization exec default local
!
!
username cisco privilege 15 secret 9 <>
!
ip ssh version 2
ip ssh authentication-retries 3
ip ssh time-out 120
!
logging history debugging
!
line vty 0 4
 transport input telnet ssh
!
netconf-yang

```

## Connecting to Netconf Port
Default Netconf Port  830
Edit Firewall Config to allow port 830
```
[cisco@atlas-jh ~]$ sudo firewall-cmd --zone=public --add-port=830/tcp --permanent
success
[cisco@atlas-jh ~]$ sudo firewall-cmd --reload
success
```

Connect from Server (Netconf Manager)
Router sends Hello with list of its capabilities
```
[cisco@atlas-jh lab]$ ssh -s cisco@172.20.1.50 -p 830 netconf
cisco@172.20.1.50's password:
<?xml version="1.0" encoding="UTF-8"?>
<hello xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
<capabilities>
</capabilities>
<session-id>41</session-id></hello>]]>]]>
```

Respond to HELLO
```
<?xml version="1.0" encoding="UTF-8"?>
<hello
    xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
    <capabilities>
        <capability>urn:ietf:params:netconf:base:1.0</capability>
    </capabilities>
</hello>]]>]]>
```
Above will leave the session open, now you can execute other commands.

## GET Running Config
XML syntax to get running config, input after hello exchange above.
```
<?xml version="1.0" encoding="UTF-8"?>
<rpc message-id="101" xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
<get-config>
<source>
<running/>
</source>
</get-config>
</rpc>]]>]]>
```
Returns full running config of router
Use https://www.xmlviewer.org/ to get Tree view of output

## Get Specific Interface Config
XML Syntax to get Interface config, input after hello exchange above
```
<filter xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
<native 
    xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native">
    <interface>
        <GigabitEthernet>
            <!-- <name>2.21</name> -->
        </GigabitEthernet>
    </interface>
</native>
```